name := "processing-examples"

version := "0.0.0"

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  "org.processing" % "core" % "2.2.1"
)

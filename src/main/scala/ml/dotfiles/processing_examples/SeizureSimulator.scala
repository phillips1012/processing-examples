package ml.dotfiles.processing_examples

import processing.core._

class SeizureSimulator extends PApplet {
  var backgroundColor = true

  override def setup =
    size(640, 360)

  override def draw = {
    if (backgroundColor) {
      background(0)
    } else {
      background(255)
    }

    backgroundColor = !backgroundColor
  }
}

package ml.dotfiles.processing_examples

object Main extends App {
  val applet = new SeizureSimulator
  val frame = new javax.swing.JFrame("Seizure Simulator")

  frame.getContentPane() add applet
  applet.init
  frame.pack
  frame setVisible true
}
